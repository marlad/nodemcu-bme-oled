# nodemcu-bme-oled

A NodeMCU (ESP8266) with a Digital Temperature/Humidity/Barometric Pressure Sensor and an OLED Display

<img src="/weather.jpg"  width="50%">

# Setup

For this setup I made use of the following:

## Arduino IDE

* Download and install the Arduino IDE from [www.arduino.cc](https://www.arduino.cc/en/Main/Software)
* Add the NodeMCU boards via Preferences -> Additional Boards Manager URLs:
`https://arduino.esp8266.com/stable/package_esp8266com_index.json`
* Select NodeMCU 1.0 ESP-12E Module via Tools -> Board
* Add the following Libraries via Sketch -> include Library -> Manage Libraries
- Adafruit BME280 Library
- Adafruit SSD1306 Library

This will pull also a few libraries for dependencies, like the Adafruit GFX Library

## SSD1306 128x32 OLED Display

<img src="/oled.jpg"  width="50%">

## ME280 Digital Temperature/Humidity/Barometric Pressure Sensor

<img src="/bme280.jpg" width="50%">

## NodeMCU (LoLin V3) ESP8266 pin schematics

![NodeMCU schematics](/nodemcu.png?raw=true)