/*
 * NodeMCU wiring schematics
 * 
 * SSD1306 OLED Display 128x32
 * gnd -> grnd
 * vcc -> 3,3v
 * sck -> D6 (GPIO12)
 * sda -> D5 (GPIO14)
 * 
 * BME280 Digital Temperature/Humidity/Barometric Pressure Sensor
 * vin -> 3.3v
 * gnd -> gnd
 * scl -> D1 (GPIO5)
 * sda -> D2 (GPIO4)
 */

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_BME280.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <elapsedMillis.h>

ESP8266WiFiMulti WiFiMulti;

#define SCREEN_WIDTH 128        // display width, in pixels
#define SCREEN_HEIGHT 32        // display height, in pixels
#define SCREEN_DATA_PIN 14      // i2c sda
#define SCREEN_CLOCK_PIN 12     // i2c scl
#define SCREEN_I2C_ADDRESS 0x3C // Address 0x3C for 128x32 display
#define BME_DATA_PIN 4          // i2c sda
#define BME_CLOCK_PIN 5         // i2c scl
#define BME_I2C_ADDRESS 0x76    // default is 0x77

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1       // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// Declaration for a BME280 connected to I2C (SDA, SCL pins)
Adafruit_BME280 bme;

// Screen delay in milliseconds
int screen_delay_long = 7000;
int screen_delay_short = 3000;

struct weatherStruct {
  double temp, pres, hum, feels, speed;
} myWeatherStruct;

class Temperature {
  public:
    double temperature, pressure, humidity;
    Temperature();
    Temperature(double t, double p, double h) : temperature(t), pressure(p), humidity(h) {}
};

class Weather: public Temperature {
  private:
    int beaufort (double wind_speed);
  public:
    // wind_speed in meter/second
    double feels_like, windspeed_in_ms;
    Weather();
    Weather(double temp, double pres, double hum, double feels, double speed): Temperature (temp, pres, hum) {
      feels_like = feels;
      windspeed_in_ms = speed;
    }
    const int windspeed() { return this->beaufort(windspeed_in_ms); }
};

// Method for converting meter/second to beaufort scale
int Weather::beaufort (double ws) {
  if(ws < 0.5) {
    return 0;
  } else if(ws >= 0.5 && ws <= 1.5) {
    return 1;
  } else if(ws >= 1.6 && ws <= 3.3) {
    return 2;
  } else if(ws >= 3.4 && ws <= 5.5) {
    return 3;
  } else if(ws >= 5.6 && ws <= 7.9) {
    return 4;
  } else if(ws >= 8.0 && ws <= 10.7) {
    return 5;
  } else if(ws >= 10.8 && ws <= 13.8) {
    return 6;
  } else if(ws >= 13.9 && ws <= 17.1) {
    return 7;
  } else if(ws >= 17.2 && ws <= 20.7) {
    return 8;
  } else if(ws >= 20.8 && ws <= 24.4) {
    return 9;
  } else if(ws >= 24.5 && ws <= 28.4) {
    return 10;
  } else if(ws >= 28.5 && ws <= 32.6) {
    return 11;
  } else if(ws >= 32.7) {
    return 12;
  }
  return -1;
}

// Replace with your SSID and password details
char ssid[] = "<SSID>";
char pass[] = "<WIFI PASSWORD>";

const String url = "http://api.openweathermap.org/data/2.5/weather?q=<CITY>,<COUNTRY_CODE>&units=metric&appid=<API_KEY>";

Weather * outside;

// Query Openweathermap api every 10 minutes, per recommendations
elapsedMillis timeElapsed;
unsigned long interval = 600000;

bool firstboot = true;

void setup() {
  int status = WL_IDLE_STATUS;
  Serial.begin(115200);

  WiFi.begin(ssid,pass);
  Serial.println("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi Connected");
  printWiFiStatus();
}

void loop() {
  Temperature inside = gettemp();
  
  if((firstboot == true) || (timeElapsed >= interval)) {

    weatherStruct ws = get_openweather_temp();
    outside = new Weather(ws.temp, ws.pres, ws.hum, ws.feels, ws.speed);

    if(firstboot == true) {
      firstboot = false;
    }
    timeElapsed = 0;
  }
  display_oled_status(inside, outside);
}

// Get the weather from openweathermap.org and return a Weather object
weatherStruct get_openweather_temp() {
  //Use http://arduinojson.org/v6/assistant/ to compute the capacity
  const size_t capacity = JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(1) + JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(4) + JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(6) + JSON_OBJECT_SIZE(13) + 290;
DynamicJsonDocument doc(capacity);

  if(WiFiMulti.run() == WL_CONNECTED) {
    HTTPClient http;
    Serial.println("[HTTP] begin...");
    http.begin(url);
    Serial.println("[HTTP] GET...");
    int httpCode = http.GET();
    if(httpCode > 0) {
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);
      if(httpCode == HTTP_CODE_OK) {
        String response = http.getString();
        //Serial.println(response);
        deserializeJson(doc, response);
        JsonObject main = doc["main"];
        JsonObject wind = doc["wind"];

        myWeatherStruct.temp = main["temp"];
        myWeatherStruct.pres = main["pressure"];
        myWeatherStruct.hum = main["humidity"];
        myWeatherStruct.feels = main["feels_like"];
        myWeatherStruct.speed = wind["speed"];

        Serial.printf("temperature outside: %.1f °C\n", myWeatherStruct.temp);
        Serial.printf("temperature feels like: %.1f °C\n", myWeatherStruct.feels);
        Serial.printf("Wind speed: %.2lf m/s\n", myWeatherStruct.speed);
      }
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }
    http.end();
  }
  return myWeatherStruct;
}

void display_oled_status(Temperature inside, Weather * outside) {

  IPAddress ip = WiFi.localIP();

  Wire.begin(SCREEN_DATA_PIN, SCREEN_CLOCK_PIN); // sda, scl

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_I2C_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
  }

  display.clearDisplay();
  display.setTextColor(SSD1306_WHITE);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(2);
  display.print(F("Inside"));
  display.setCursor(0, 16);
  display.print(F("temperature"));
  display.display();
  delay(screen_delay_short);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(4);
  display.printf("%.1f", inside.temperature);
  display.setCursor(110, 0);
  display.setTextSize(1);
  display.printf("C");
  display.display();
  delay(screen_delay_long);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(2);
  display.print(F("Inside"));
  display.setCursor(0, 16);
  display.print(F("humidity"));
  display.display();
  delay(screen_delay_short);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(4);
  display.printf("%.1f", inside.humidity);
  display.setCursor(110, 0);
  display.setTextSize(1);
  display.printf("%%");
  display.display(); 
  delay(screen_delay_long);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(2);
  display.print(F("Outside"));
  display.setCursor(0, 16);
  display.print(F("temperature"));
  display.display();
  delay(screen_delay_short);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(4);
  display.printf("%.1f", outside->temperature);
  display.setCursor(110, 0);
  display.setTextSize(1);
  display.print(F("C"));
  display.display();
  delay(screen_delay_long);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(2);
  display.print(F("Feels"));
  display.setCursor(0, 16);
  display.print(F("like"));
  display.display();
  delay(screen_delay_short);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(4);
  display.printf("%.1f", outside->feels_like);
  display.setCursor(110, 0);
  display.setTextSize(1);
  display.print(F("C"));
  display.display();
  delay(screen_delay_long);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(2);
  display.print(F("Wind"));
  display.setCursor(0, 16);
  display.print(F("speed"));
  display.setCursor(72, 0);
  display.setTextSize(4);
  display.printf("%d", outside->windspeed());
  display.setCursor(110, 0);
  display.setTextSize(1);
  display.print(F("Bft"));
  display.display();
  delay(screen_delay_long);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(2);
  display.print(F("Outside"));
  display.setCursor(0, 16);
  display.print(F("humidity"));
  display.display();
  delay(screen_delay_short);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(4);
  display.printf("%.1f", outside->humidity);
  display.setCursor(110, 0);
  display.setTextSize(1);
  display.printf("%%");
  display.display();
  delay(screen_delay_long);
}

// Get temperature from a BME280 sensor and return a Temperature object
Temperature gettemp() {
  Temperature * tempData;
  
  Wire.begin(BME_DATA_PIN, BME_CLOCK_PIN); // sda, scl
  if(!bme.begin(BME_I2C_ADDRESS)) {
    Serial.println(F("Could not find a valid BME280 sensor."));
  }

  tempData = new Temperature (bme.readTemperature(), bme.readPressure() / 100.0F, bme.readHumidity());

  Serial.print("temperature: ");
  Serial.print(tempData->temperature);
  Serial.println(" °C");

  Serial.print("humidity: ");
  Serial.print(tempData->humidity);
  Serial.println(" %");

  Serial.print("pressure: ");
  Serial.print(tempData->pressure);
  Serial.println(" mbar");
  
  return * tempData;
}

void displayscroll(void) {
  int randNumber = random(3);

  switch(randNumber) {
    case 0: display.startscrollright(0x00, 0x0F);
            break;
    case 1: display.startscrollleft(0x00, 0x0F);
            break;
    case 2: display.startscrolldiagright(0x00, 0x07);
            break;
    case 3: display.startscrolldiagleft(0x00, 0x07);
            break;
  }
  delay(4000);
  display.stopscroll();
}

void printWiFiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
